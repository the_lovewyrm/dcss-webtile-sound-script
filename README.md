# DCSS_Sound4Webtiles
Note: I'll be working on wrapping this up into a proper addon to, hopefully, circumvent all the hoops regarding sound assets and co.
Consider this version pretty much frozen at this point.

Hacky cobble-y-fication of the Korean webtiles sound script realized as a GreaseMonkey script.

Disclaimer: I am not a WebDev. if you are then you can probably do better than this.

This addon will eat up your RAM if you refresh the page too much, I am too inexperienced to fix this.
But it's fine if you do it in moderation, the culprit seems to be the audio module, its buffers, to be precise.
If your RAM gets consumed too much then you'll either have to restart your browser, or close all DCSS tabs and do a memory minimize.

For Firefox this is done via "about:memory", which opens up a page that has buttons for garbage collection, cycle collection and memory minimization.

Anyway.
Opted to load all the sounds immediately (and to play them immediately) and aimed for making them persistent, because in the first version of this thing sounds wouldn't play immediately and the buffers had to take a while to be filled.

I went for the route of embedding the sound files as base64 strings, which bloats the script and makes it a pain to edit and possibly view, if your text editor sucks, but it loads quite snappily.
Plus, it doesn't require any cross domain hackery, which seem to be a security risk.

Kept the code at the top, the big bloaty stuff is at the bottom. Be sure to use a text editor, or text editor setting that can disable linewrapping.

## Installation

1. Make sure you have user scripts enabled in your browser (these instructions refer to the latest versions of the browser):

	* Firefox - install [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/).
	* Chrome - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=chrome).
	* Opera - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=opera) or [Violent Monkey](https://addons.opera.com/en/extensions/details/violent-monkey/).
	* Safari - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=safari).
	* Dolphin - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=dolphin).
	* UC Browser - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=ucweb).

## Usage

Project 357 users can, at this point at least, use "/soundon", and "/soundoff" to enable or disable the sound. There are no notifications aside from console log entries.
This will not fill up your RAM, unless you reload the page and enable sounds again, then again reload, repeat. Turning it on and off on its own will not drain your RAM.


To enable sound on other servers, at this point, add "?sound=true" to the end of your lobby and hit Enter.

For example:
http://crawl.berotato.org:8080/#lobby

becomes
http://crawl.berotato.org:8080/#lobby?sound=true

That will reload the lobby and load the sounds. 
Note: Again, I am not a webdev, so this solution might be crappy but using these kind of suffixes seems to be the only unversal way of doing it.

You can also add sounds to a running game by adding "?sound=true" at the end of your game URL, too.
But loading the sounds in the lobby has the little nuance that you get a little sound upon starting the game.

You could also create a bookmark with "?sound=true" at the end and use that directly.

In fact, it seems that reloading an already loaded lobby or game might not work out with this, so if you really want to be sure, start from a clean tab and use a bookmark, or something.

## Adding Sound Matches

I have opened an Issue in the Issuetracker for that kinda thing. 
You can also find all the sounds used in a zipfile for reference.

## Script customization

Greasemonkey, and possibly Tampermonkey will allow you to edit the script if you right click in the Grease/Tampermonkey drop down menu.
You can use this to adjust things like "generic death sounds" or "maximum number of sounds that can play at the same time".

Optimally, this will be replaced by a proper web interface...someday.
